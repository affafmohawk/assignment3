<h1>College Address Book</h1>
<table id="myTable" class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>City</th>
    </tr>
    </thead>
    <tbody>
    <?php
        renderTable($data);
    ?>
    </tbody>
</table>
<button type="button" id="add" class="btn btn-default" aria-label="Left Align">
    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
</button> Add new Contact
